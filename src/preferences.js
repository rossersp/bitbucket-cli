var Promise = require('bluebird'),
    path = require('path'),
    jsonfile = Promise.promisifyAll(require('jsonfile')),
    _ = require('lodash'),
    fs = Promise.promisifyAll(require('fs'));

module.exports = exports = function() {
    return Promise.all(process.cwd()
            .split(path.sep)
            .map(function(value, ix, col) {
                var candidatePath = path.resolve(col.slice(0, col.length - ix)
                    .join(path.sep));
                //return the bbcli rc on the candidate path if it exists,
                //return the git directory on the candidate path if it exsists,
                //use _.noop to consume stat problems
                return Promise.all([fs.statAsync(path.resolve(candidatePath, '.bbcli.rc'))
                        .then(function() {
                            return path.resolve(candidatePath, '.bbcli.rc');
                        })
                        .error(_.noop),
                        fs.statAsync(path.resolve(candidatePath, '.git'))
                        .then(function() {
                            return path.resolve(candidatePath, '.bbcli.rc');
                        })
                        .error(_.noop)
                    ])
                    .spread(function(bbcli, git) {
                        return {
                            git: git,
                            bbcli: bbcli
                        };
                    })
            }))
        .reduce(function(a, f) {
            //reduce the closest ancestor and git ancestor down to a single object
            a.ancestor = a.ancestor || f.git;
            a.gitancestor = a.gitancestor || f.bbcli;
            return a;
        }, {})
        .then(function(p) {
            return fs.statAsync(path.resolve(__dirname, '.bbcli.rc'))
                .then(function() {
                    //rc exists.
                    return p;
                })
                .error(function() {
                    //make the rc
                    jsonfile.writeFileSync(path.resolve(__dirname, '.bbcli.rc'), {});
                    return p;
                });
        })
        .then(function(p) {
            return jsonfile.readFileAsync(p.ancestor ||'')
                .then(function(object) {
                    //if the file does exist, set the local preferences
                    p.localPreferences = object;
                    return p;
                })
                .error(function(err) {
                    //if the file does not exist, ignore the fs error
                    return p;
                });
        })
        .then(function(p) {
            //get the global prefs
            var globalPreferences = jsonfile.readFileSync(path.resolve(__dirname, '.bbcli.rc')),
                localPreferences = p.localPreferences || {};
            //build the environment preferences
            var environmentPreferences = _.reduce(process.env, function(thing, value, key) {
                _.set(thing, key, value);
                return thing;
            }, {});

            //construct the preferences object
            var preferences = {};
            preferences = _.defaultsDeep(preferences, environmentPreferences);
            //prefer environment to local
            preferences = _.defaultsDeep(preferences, localPreferences);
            //prefer local to global
            preferences = _.defaultsDeep(preferences, globalPreferences);
            //prefer global to internal
            preferences = _.defaultsDeep(preferences, {
                issues: {
                    kind: 'enhancement',
                    priority: 'major',
                    status: 'new'
                }
            });

            //add ancestry to preferences
            preferences.gitancestor = path.resolve(p.gitancestor.split(path.sep)
                .slice(0, p.gitancestor.split(path.sep)
                    .length - 1)
                .join(path.sep), '.bbcli.rc');
            if (!p.ancestor) {
                preferences.ancestor = preferences.gitancestor;
            } else {
                var toAncestor = path.relative(process.cwd(), p.ancestor);
                var toGitAncestor = path.relative(process.cwd(), preferences.gitancestor);
                if (toGitAncestor.length <= toAncestor.length) {
                    preferences.ancestor = preferences.gitancestor;
                } else {
                    preferences.ancestor = p.ancestor;
                }
            }

            //add write method
            preferences.write = function(value, member, location) {
                location = location || 'local';
                if (/^local$/i.test(location)) {
                    _.set(localPreferences, member, value);
                    jsonfile.writeFileSync(preferences.ancestor, localPreferences);
                } else if (/^global$/i.test(location)) {
                    _.set(globalPreferences, member, value);
                    jsonfile.writeFileSync(path.resolve(__dirname, '.bbcli.rc'), globalPreferences);
                } else {
                    throw new Error('preference write location must be local or global');
                }
            }
            return preferences;
        });
};
