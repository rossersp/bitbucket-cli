var Promise = require('bluebird'),
    path = require('path'),
    prompt = Promise.promisifyAll(require('prompt')),
    jsonfile = require('jsonfile'),
    _ = require('lodash'),
    preferences = require('./preferences'),
    fs = Promise.promisifyAll(require('fs'));

module.exports = exports = function(schema, options) {
    //don't prompt for specified options
    var specified = {
        properties: {}
    };
    _.each(options, function(value, key) {
        if (!!value) {
            specified.properties[key] = schema.properties[key];
            delete schema.properties[key];
        };
    });

    if (!options || (!!options && !options.noprompt)) {
        //add schemas for saving
        _.each(schema.properties, function(value, key) {
            if (value.promptSave && !schema.properties[key].save) {
                schema.properties[key + 'Save'] = {
                    pattern: /^y(?:es)?|n(?:o)?$/i,
                    description: 'Commit ' + key + ' to preferences?',
                    message: 'Enter yes, no, y, or n.',
                    required: true,
                };
            }
        });

        if (!!options.save) {
            //add a question about where to save
            schema.properties['preferenceType'] = {
                pattern: /^local|global$/i,
                description: 'Preference type (local|global)',
                message: 'Enter local or global',
                required: true,
            };
        }

        return preferences().then(function(preferences){
            return prompt.getAsync(schema).then(function(r){
                return {
                    preferences: preferences,
                    result:r
                }
            });
        })
            .then(function(r) {
                //make sure the required schema properties exist in the result
                _.each(schema.properties, function(value, key) {
                    if (schema.properties[key].required && !r.result[key]) {
                        throw new Error({
                            argument: key,
                            message: key + ' must be specified.'
                        });
                    }
                });

                _.each(r.result, function(value, key) {
                    if (!!schema.properties[key].save || /^y/i.test(r[key + 'Save'])) {
                        r.preferences.write(_.get(r.result, key), key, r.result.preferenceType);
                    }
                });
                return r;
            })
            .then(function(r) {
                //make sure the schema values are filled in on the options
                return _.defaults(options, r.result);
            });
    } else {
        return options;
    }
};
