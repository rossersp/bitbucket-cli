var program = require('commander');
var package = require('../package');
var prompt = require('prompt');
prompt.start();

program
    .version(package.version)
    .description(package.description)
    .action(function() {
        require('./preferences')()
            .then(function(preferences) {
                program.outputHelp();
                if (!preferences.bitbucketKey) {
                    logger.warn('Bitbucket key not set.  Commands will fail until this is corrected.');
                }
                if (!preferences.bitbucketSecret) {
                    logger.warn('Bitbucket secret not set.  Commands will fail until this is corrected.');
                }
            });
    });

var commands = require('path')
    .join(__dirname, 'commands');
require('fs')
    .readdirSync(commands)
    .forEach(function(file) {
        require('./commands/' + file);
    });

program.parse(process.argv);


if (!process.argv.slice(2)
    .length) {
    require('./preferences')()
        .then(function(preferences) {
            program.outputHelp();
            if (!preferences.bitbucketKey) {
                logger.warn('Bitbucket key not set.  Commands will fail until this is corrected.');
            }
            if (!preferences.bitbucketSecret) {
                logger.warn('Bitbucket secret not set.  Commands will fail until this is corrected.');
            }
        });
}
