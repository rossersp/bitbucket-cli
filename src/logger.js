var chalk = require('chalk'),
    moment = require('moment'),
    urlencode = require('urlencode'),
    util = require('util');

var themes = {
    title: chalk.blue.bold.underline,
    normal: chalk.white,
    info: chalk.cyan,
    silly: chalk.magenta,
    error: chalk.red,
    success: chalk.green,
    warning: chalk.yellow,
    href: chalk.blue.underline
};

function log(label, value, theme) {
    theme = theme || themes.info;
    console.log(theme(label + ':'), value);
}

module.exports.title = function(title, ref) {
    if (ref) {
        console.log(themes.title.cyan('[' + ref + ']'), themes.title(title));
        return;
    }
    console.log(themes.title(title));
};

module.exports.issue = {};
module.exports.issue.title = function(priority, title, localId, kind, status) {
    var priorityGlyph = '',
        color = chalk.white;

    if (priority == 'blocker') {
        priorityGlyph = '↑↑↑';
        color = chalk.white.bgRed.bold;
    } else if (priority == 'critical') {
        priorityGlyph = '↑↑';
        color = chalk.red.underline;
    } else if (priority == 'major') {
        priorityGlyph = '↑';
        color = chalk.red;
    } else if (priority == 'minor') {
        priorityGlyph = '↓';
        color = chalk.green;
    } else if (priority = 'trivial') {
        priorityGlyph = '↓↓';
        color = chalk.green.dim;
    }
    console.log(color('%s #%s | ' +module.exports.status(status)+ ' | '+ module.exports.kind(kind) +': %s'), priorityGlyph, localId, title);
};

module.exports.comment = {}
module.exports.comment.display = function(comment, options) {
    console.log(chalk.blue.bold('#%s | On %s, %s %s (%s) said:'),
        comment.comment_id,
        moment(comment.utc_updated_on)
        .format('MM/DD/YYYY [at] h:mm a'),
        comment.author_info.first_name,
        comment.author_info.last_name,
        comment.author_info.username);
    console.log(chalk.white(comment.content));
    console.log('\n');
};

module.exports.issue.display = function(issue, options) {
    var logger = module.exports;
    if (!!options && !!options.abbreviated) {
        logger.issue.title(issue.priority, issue.title, issue.local_id, issue.metadata.kind, issue.status);
    } else {
        console.log('\n');
        logger.issue.title(issue.priority, issue.title, issue.local_id, issue.metadata.kind, issue.status);
        logger.info('Reported By',
            issue.reported_by.first_name + ' ' + issue.reported_by.last_name + ' (' + issue.reported_by.username + ')');
        logger.info('Last Updated', moment(issue.utc_last_updated)
            .format('MM/DD/YYYY [at] h:mm a'));

        if (!!issue.responsible) {
            logger.info('Assigned to',
                issue.responsible.first_name + ' ' + issue.responsible.last_name + ' (' + issue.responsible.username + ')');
        } else {
            logger.info('Assigned to', 'unassigned')
        }
        logger.info('Comments', issue.comment_count || 0);
        console.log(chalk.green.bold('\nDescription:'));
        if (/^(?:\s*)$/.test(urlencode.decode(issue.content || ''))) {
            console.log(chalk.white('No description provided.'));
        } else {
            console.log(chalk.white(urlencode.decode(issue.content)));
        }
        console.log('\n');

        if (!!issue.comments) {
            console.log(chalk.green.bold('Comments'));
            if (issue.comments.length == 0) {
                console.log(chalk.white('No comments found.'));
                console.log('\n');
            }
            issue.comments.forEach(function(comment) {
                logger.comment.display(comment);
            });

        }
    }
};

module.exports.status = function(status) {
    var color = chalk.red;
    if (status == 'new') {
        color = chalk.blue.bold;
    } else if (status == 'open') {
        color = chalk.blue;
    } else if (status == 'resolved') {
        color = chalk.green;
    } else if (status == 'wontfix') {
        color = chalk.red;
    } else if (status == 'invalid') {
        color = chalk.white.bgRed;
    }

    return color(status);
};

module.exports.kind= function(kind) {
    var color = chalk.red;
    if (kind == 'bug') {
        color = chalk.white.bgRed
    } else if (kind == 'enhancement') {
        color = chalk.green;
    } else if (kind == 'task') {
        color = chalk.blue;
    } else if (kind == 'proposal') {
        color = chalk.yellow;
    }

    return color(kind);
};

module.exports.silly = function(label, value) {
    log(label, value, themes.silly);
};

module.exports.info = function(label, value) {
    log(label, value, themes.info);
};

module.exports.success = function(message) {
    log('Success', message, themes.success);
};

module.exports.error = function(err) {
    log('Error', err.message, themes.error);
    module.exports.silly('error detail', err.stack);
};

module.exports.warn = function(message) {
    log('Warning', message, themes.warning);
};


module.exports.href = function(href) {
    console.log(themes.href(href));
};

module.exports.pagination = function(prefix, start, end, total){
    console.log(chalk.cyan.bold(prefix + ' |') + ' %s to %s of %s', start, end, total);
};
