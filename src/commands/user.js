var program = require('commander'),
    api = require('../api'),
    logger = require('../logger'),
    promptSave = require('../promptSave');

function getUser(username, opts) {
    api.v2.users.get(username)
        .then(function(body) {
            logger.info('Username', body.username);
            logger.info('Name', body.display_name);

            if (body.location) {
                logger.info('Location', body.location);
            }

            if (body.website) {
                logger.info('Website', body.website);
            }

            if (opts.browse) {
                logger.href(body.links.html.href);
            }
        })
        .catch(logger.error);
}

program
    .command('user [username]')
    .description('')
    .option('-b, --browse', 'Show href to browse resource')
    .option('-f --noprompt', 'Skip prompts for saving')
    .option('--save', 'Save options')
    .action(function(username, options) {
        options.username = username;
        require('../preferences')()
            .then(function(preferences) {
                return promptSave({
                    properties: {
                        username: {
                            pattern: /^[\w\-]+$/,
                            description: 'Enter the username',
                            message: 'Username must be supplied.',
                            required: true,
                            promptSave: !options.username && !options.noprompt,
                            save: !!options.save
                        },
                    }
                }, options);
            })
            .then(function(options) {
                return api.v2.users.get(options.username);
            })
            .then(function(body) {
                logger.info('Username', body.username);
                logger.info('Name', body.display_name);

                if (body.location) {
                    logger.info('Location', body.location);
                }

                if (body.website) {
                    logger.info('Website', body.website);
                }

                if (options.browse) {
                    logger.href(body.links.html.href);
                }
            })
            .catch(logger.error);

    });
