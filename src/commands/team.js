var program = require('commander'),
    api = require('../api'),
    logger = require('../logger'),
    preferences = require('../preferences'),
    promptSave = require('../promptSave');

program
    .command('team [teamName]')
    .description('')
    .option('-b, --browse', 'Show href to browse resource')
    .option('-f --noprompt', 'Skip prompts for saving')
    .option('--save', 'Save options')
    .action(function(teamName, options) {
        options.teamName = teamName;
        require('../preferences')()
            .then(function(preferences) {
                return promptSave({
                    properties: {
                        teamName: {
                            pattern: /^[\w\-]+$/,
                            description: 'Enter the team',
                            message: 'Team must be supplied.',
                            required: true,
                            promptSave: !options.teamName && !options.noprompt,
                            save: !!options.save
                        },
                    }
                }, options);
            })
            .then(function(options) {
                return api.v2.teams.get(options.teamName);
            })
            .then(function(body) {
                logger.info('Username', body.username);
                logger.info('Name', body.display_name);

                if (options.browse) {
                    logger.href(body.links.html.href);
                }
            })
            .catch(logger.error);
    });
