var program = require('commander'),
    moment = require('moment'),
    promptSave = require('../promptSave'),
    api = require('../api'),
    logger = require('../logger');

function outputPullRequest(pullRequest, showDetail, opts) {
    logger.title(pullRequest.title, pullRequest.id);
    logger.info('Author', pullRequest.author.username);

    if (showDetail) {
        var created = moment(pullRequest.created_on);
        logger.info('Created', created.format('DD/MM/YYYY HH:mm'));

        var updated = moment(pullRequest.updated_on);
        logger.info('Updated', updated.format('DD/MM/YYYY HH:mm'));
    }

    logger.info('Source Branch', pullRequest.source.branch.name);
    logger.info('Destination Branch', pullRequest.destination.branch.name);

    if (showDetail) {
        logger.info('Close Source', pullRequest.close_source_branch);
    }

    logger.info('State', pullRequest.state);

    if (showDetail && opts.browse) {
        logger.href(pullRequest.links.html.href);
    }
}

program
    .command('pull-requests [id]')
    .description('')
    .option('-p, --page <page>', 'Set page number')
    .option('-l, --limit <limit>', 'Set page limit')
    .option('-b, --browse', 'Show href to browse resource')
    .option('-f --noprompt', 'Skip prompts for saving')
    .option('-o, --owner <owner>', 'Specifies the owner')
    .option('-s, --slug <slug>', 'Specifies the project slug')
    .option('--save', 'Save options')
    .action(function(id, options) {
        options.id = id;
        require('../preferences')()
            .then(function(preferences) {
                return promptSave({
                    properties: {
                        owner: {
                            pattern: /^\w+$/,
                            description: 'Enter the owner',
                            message: 'Owner must be supplied.',
                            required: true,
                            promptSave: !options.owner && !options.noprompt,
                            save: !!options.save
                        },
                        slug: {
                            pattern: /^[\w\-]+$/,
                            description: 'Enter the project slug',
                            message: 'Slug must be supplied.',
                            required: true,
                            promptSave: !options.slug && !options.noprompt,
                            save: !!options.save
                        },
                        id: {
                            description: 'Enter the pull request ID (leave blank for list)',
                            required: false,
                            promptSave: false,
                            save: false
                        },
                    }
                }, options);
            })
            .then(function(options) {
                return api.v2.pullRequests.get(options.owner, options.slug, options.id, options);
            })
            .then(function(body) {
                if (Array.isArray(body.values)) {
                    if (body.values.length == 0) {
                        logger.success('No pull requests found.');
                    } else {
                        body.values.forEach(function(pr) {
                            outputPullRequest(pr, false, opts);
                        });
                    }
                    return;
                }
                outputPullRequest(body, true, opts);
            })
            .catch(logger.error)
    });
