var program = require('commander'),
    api = require('../../api'),
    logger = require('../../logger'),
    prompt = require('prompt'),
    promptSave = require('../../promptSave'),
    async = require('async'),
    chalk = require('chalk'),
    moment = require('moment'),
    _ = require('lodash'),
    util = require('util');

program
    .command('issue [issue]')
    .option('-o, --owner <owner>', 'Specifies the owner')
    .option('-s, --slug <slug>', 'Specifies the project slug')
    .option('--save', 'save options to preferences')
    .option('-f --noprompt', 'skip prompts for saving')
    .option('-c --comments', 'get comments for the issue')
    .description('Get specific issue information.')
    .action(function(issue, options) {
        options.issue = issue;
        require('../../preferences')()
            .then(function(preferences) {
                options = _.defaults(options, preferences);
                return promptSave({
                    properties: {
                        owner: {
                            pattern: /^\w+$/,
                            description: 'Enter the owner',
                            message: 'Owner must be supplied.',
                            required: true,
                            promptSave: !options.owner && options.noprompt,
                            save: !!options.save
                        },
                        slug: {
                            pattern: /^[\w\-]+$/,
                            description: 'Enter the project slug',
                            message: 'Slug must be supplied.',
                            required: true,
                            promptSave: !options.slug && options.noprompt,
                            save: !!options.save
                        },
                        issue: {
                            pattern: /^\w+$/,
                            description: 'Enter the issue ID',
                            message: 'Issue ID must be supplied..',
                            required: true,
                            promptSave: false,
                            save: false
                        }
                    }
                }, options)
            })
            .then(function(options) {
                return api.v1.issues.get(options.owner, options.slug, options.issue)
            })
            .then(function(issue) {
                if (!!options.comments) {
                    return api.v1.issues.comments.list(options.owner, options.slug, options.issue)
                        .then(function(comments) {
                            issue.comments = comments;
                            return issue;
                        });
                }
                return issue;
            })
            .then(function(issue) {
                logger.issue.display(issue);
            })
            .error(function(err) {
                logger.error(err);
            })
            .catch(function(err) {
                logger.error(err);
            });
    });
