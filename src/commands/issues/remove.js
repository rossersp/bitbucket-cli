var program = require('commander'),
    api = require('../../api'),
    logger = require('../../logger'),
    prompt = require('prompt'),
    promptSave = require('../../promptSave'),
    async = require('async'),
    chalk = require('chalk'),
    moment = require('moment'),
    _ = require('lodash'),
    util = require('util');

program
    .command('remove-issue [issue]')
    .option('--save', 'Save options to preferences')
    .option('-o, --owner <owner>', 'Specifies the owner')
    .option('-s, --slug <slug>', 'Specifies the project slug')
    .option('-f --noprompt', 'Skip prompts for saving')
    .description('Removes an issue in a repository.')
    .action(function(issue, options) {
        options.issue = issue;
        require('../../preferences')()
            .then(function(preferences) {
                options = _.defaults(options, preferences);
                return promptSave({
                    properties: {
                        owner: {
                            pattern: /^\w+$/,
                            description: 'Enter the owner',
                            message: 'Owner must be supplied.',
                            required: true,
                            promptSave: !options.owner && options.noprompt,
                            save: !!options.save
                        },
                        slug: {
                            pattern: /^[\w\-]+$/,
                            description: 'Enter the project slug',
                            message: 'Slug must be supplied.',
                            required: true,
                            promptSave: !options.slug && options.noprompt,
                            save: !!options.save
                        },
                        issue: {
                            pattern: /^\w+$/,
                            description: 'Enter the issue number',
                            message: 'Issue number must be supplied.',
                            required: true,
                            promptSave: false,
                            save: false
                        },
                    }
                }, options);
            })
            .then(function(options) {
                return api.v1.issues.remove(options.owner, options.slug, options.issue)
            })
            .then(function(issue) {
                logger.success('Issue ' + issue + ' deleted successfully.\n');
            })
            .error(function(err) {
                logger.error(err);
            })
            .catch(function(err) {
                logger.error(err);
            });
    });
