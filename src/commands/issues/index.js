module.exports = exports = {
	"comments": require("./comments"),
	"create": require("./create"),
	"get": require("./get"),
	"list": require("./list"),
	"remove": require("./remove"),
	"update": require("./update"),
};
