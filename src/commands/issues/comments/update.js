var program = require('commander'),
    api = require('../../../api'),
    logger = require('../../../logger'),
    prompt = require('prompt'),
    promptSave = require('../../../promptSave'),
    async = require('async'),
    chalk = require('chalk'),
    moment = require('moment'),
    _ = require('lodash'),
    util = require('util');

program
    .command('update-comment [issue] [comment]')
    .description('Update a comment on an issue in  a repository.')
    .option('--save', 'Save options to preferences')
    .option('-f --noprompt', 'Skip prompts for saving')
    .option('-c --content <comment>', 'Comment to add to issue')
    .option('-o, --owner <owner>', 'Specifies the owner')
    .option('-s, --slug <slug>', 'Specifies the project slug')

.action(function(issue, comment, options) {
    options.issue = issue;
    options.comment = comment;
    require('../../../preferences')()
        .then(function(preferences) {
            options = _.defaults(options, preferences);
            return promptSave({
                properties: {
                    owner: {
                        pattern: /^\w+$/,
                        description: 'Enter the owner',
                        message: 'Owner must be supplied.',
                        required: true,
                        promptSave: !options.owner && options.noprompt,
                        save: !!options.save
                    },
                    slug: {
                        pattern: /^[\w\-]+$/,
                        description: 'Enter the project slug',
                        message: 'Slug must be supplied.',
                        required: true,
                        promptSave: !options.slug && options.noprompt,
                        save: !!options.save
                    },
                    issue: {
                        pattern: /^\w+$/,
                        description: 'Enter the issue',
                        message: 'Issue number must be supplied.',
                        required: true,
                        promptSave: false,
                        save: false
                    },
                    issue: {
                        pattern: /^\w+$/,
                        description: 'Enter the issue',
                        message: 'Issue number must be supplied.',
                        required: true,
                        promptSave: false,
                        save: false
                    },
                    comment: {
                        pattern: /^\w+$/,
                        description: 'Enter the comment',
                        message: 'Comment number must be supplied.',
                        required: true,
                        promptSave: false,
                        save: false
                    },
                    content: {
                        description: 'Enter your comment',
                        message: 'Comment content must be supplied.',
                        required: true,
                        promptSave: false,
                        save: false
                    },
                }
            }, options);
        })
        .then(function(options) {
            return api.v1.issues.comments.update(options.owner, options.slug, options.issue, options.comment, options);
        })
        .then(function(comment) {
            logger.comment.display(comment);
        })
        .error(function(err) {
            logger.error(err);
        })
        .catch(function(err) {
            logger.error(err);
        });
});
