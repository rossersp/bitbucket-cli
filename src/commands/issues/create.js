var program = require('commander'),
    api = require('../../api'),
    logger = require('../../logger'),
    promptSave = require('../../promptSave'),
    chalk = require('chalk'),
    moment = require('moment'),
    _ = require('lodash'),
    util = require('util');

program
    .command('create-issue')
    .description('Create an issue in a repository.')
    .option('--save', 'Save options to preferences')
    .option('-f --noprompt', 'Skip prompts for saving')
    .option('-t --title <title>', 'Give a title to the issue being created')
    .option('-d --issueDescription <issueDescription>', 'Give a description for the issue being created')
    .option('-p --priority <priority>', 'Specify the priority of an issue')
    .option('-a --assignee <assignee>', 'Assign the created issue')
    .option('-k --kind <kind>', 'Assign a kind to the issue')
    .option('-s --status <status>', 'Assign a status to the issue')
    .option('--component <component>', 'Assign the issue to a component')
    .option('--milestone <milestone>', 'Assign the issue to a milestone')
    .option('-o, --owner <owner>', 'Specifies the owner')
    .option('-s, --slug <slug>', 'Specifies the project slug')
//    .option('--version <version>', 'Assign the issue to a version')

.action(function(options) {
    require('../../preferences')()
        .then(function(preferences) {
            options = _.defaults(options, preferences);
            options = _.defaults(options, options.issues);
            return promptSave({
                properties: {
                    owner: {
                        pattern: /^\w+$/,
                        description: 'Enter the owner',
                        message: 'Owner must be supplied.',
                        required: true,
                        promptSave: !options.owner && !options.noprompt,
                        save: !!options.save
                    },
                    slug: {
                        pattern: /^[\w\-]+$/,
                        description: 'Enter the project slug',
                        message: 'Slug must be supplied.',
                        required: true,
                        promptSave: !options.slug && !options.noprompt,
                        save: !!options.save
                    },
                    title: {
                        description: 'Enter the issue title',
                        message: 'Issue title must be supplied.',
                        required: true,
                        promptSave: false,
                        save: false
                    },
                    issueDescription: {
                        description: 'Enter the issue description',
                        required: false,
                        promptSave: false,
                        save: false
                    },
                    assignee: {
                        pattern: /^\w+$/,
                        description: 'Enter the name of the assignee',
                        required: false,
                        promptSave: false,
                        save: false
                    },
                }
            }, options)

        })
        .then(function(options) {
            return api.v1.issues.create(options.owner, options.slug, options)
        })
        .then(function(issue) {
            logger.issue.display(issue);
        })
        .error(function(err) {
            logger.error(err);
        })
        .catch(function(err) {
            logger.error(err);
        });
});
