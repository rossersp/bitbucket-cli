var program = require('commander'),
    api = require('../../api'),
    logger = require('../../logger'),
    prompt = require('prompt'),
    promptSave = require('../../promptSave'),
    async = require('async'),
    chalk = require('chalk'),
    moment = require('moment'),
    _ = require('lodash'),
    util = require('util');

program
    .command('issues')
    .option('-o, --owner <owner>', 'Specifies the owner')
    .option('-s, --slug <slug>', 'Specifies the project slug')
    .option('-l, --limit <limit>', 'Sets number of records to return')
    .option('-b, --start <start>', 'Offset to start at')
    .option('-q, --search <search>', 'A string to search for')
    .option('--sort <sort>', 'The field to sort issues by')
    .option('--title <title>', 'Contains a filter operation to restrict the list of issues')
    .option('--content <content>', 'Contains a filter operation to restrict the list of issues')
//.option('--version <version>', 'The version to restrict to')
.option('--milestone <milestone>', 'The milestone to restrict to')
    .option('--component <component>', 'The component to restrict to')
    .option('--kind <kind>', 'The kind to restrict to')
    .option('--status <status>', 'The status to restrict to')
    .option('--responsible <responsible>', 'Filter to who is responsible')
    .option('--reportedBy <reportedBy>', 'Filter to who issue was reported by')
    .option('--save', 'save options to preferences')
    .option('-f --noprompt', 'skip prompts for saving')
    .option('-a --abbreviated', 'show an abbreviated list')
    .option('--all', 'show all issues - overrides limit if specified')
    .description('Show issues for a given project.')
    .action(function(options) {
        require('../../preferences')()
            .then(function(preferences) {
                options = _.defaults(options, preferences);
                return promptSave({
                    properties: {
                        owner: {
                            pattern: /^\w+$/,
                            description: 'Enter the owner',
                            message: 'Owner must be supplied.',
                            required: true,
                            promptSave: !options.owner && options.noprompt,
                            save: !!options.save
                        },
                        slug: {
                            pattern: /^[\w\-]+$/,
                            description: 'Enter the project slug',
                            message: 'Slug must be supplied.',
                            required: true,
                            promptSave: !options.slug && options.noprompt,
                            save: !!options.save
                        }
                    }
                }, options)
            })
            .then(function(options) {
                return api.v1.issues.list(options.owner, options.slug, options)
            })
            .then(function(body) {
                body.issues.forEach(function(issue) {
                    logger.issue.display(issue, options);
                });
                logger.pagination('issues',  options.start || 1, (parseInt(options.start || 0, 10)) + body.issues.length, body.count);
            })
            .error(function(err) {
                logger.error(err);
            })
            .catch(function(err) {
                logger.error(err);
            });
    });
