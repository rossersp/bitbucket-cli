var program = require('commander');
var api = require('../api');
var logger = require('../logger');
var promptSave = require('../promptSave');

program
    .command('create-repository [owner] [slug]')
    .alias('create-repo')
    .description('Create a repository.')
    .option('-n, --name <name>', 'Set repository name')
    .option('-d, --repoDescription <description>', 'Set repository description')
    .option('-p, --privacy', 'Make the repo private')
    .option('-s, --scm <scm>', 'Set repository SCM')
    .option('-f, --noprompt', 'Skip prompts')
    .option('--save', 'Save options')
    .action(function(owner, slug, options){
        options.owner = owner;
        options.slug = slug;
        require('../preferences')()
            .then(function(preferences) {
                return promptSave({
                    properties: {
                        owner: {
                            pattern: /^\w+$/,
                            description: 'Enter the owner',
                            message: 'Owner must be supplied.',
                            required: true,
                            promptSave: !options.owner && !options.noprompt,
                            save: !!options.save
                        },
                        slug: {
                            pattern: /^[\w\-]+$/,
                            description: 'Enter the project slug',
                            message: 'Slug must be supplied.',
                            required: true,
                            promptSave: !options.slug && !options.noprompt,
                            save: !!options.save
                        },
                        repoDescription: {
                            description: 'Enter the project description',
                            required: false,
                            promptSave: false,
                            save: false
                        },
                        name: {
                            description: 'Enter the project name',
                            required: false,
                            promptSave: false,
                            save: false
                        },
                        scm: {
                            pattern: /^git|hg$/i,
                            description: 'Enter the project SCM',
                            required: true,
                            promptSave: false,
                            save: false
                        },
                        privacy: {
                            pattern: /^public|private$/i,
                            description: 'Enter the privacy setting (public|private)',
                            required: true,
                            promptSave: false,
                            save: false
                        }
                    }
                }, options);
            })
            .then(function(options) {
                return api.v2.repositories.create(options.owner, options.slug, options)
            })
            .then(function(body) {
                logger.success('Repository successfully created');
            })
            .catch(logger.error);
    });
