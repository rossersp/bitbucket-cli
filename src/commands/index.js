module.exports = exports = {
	"create-repository": require("./create-repository"),
	"issues": require("./issues"),
	"pull-requests": require("./pull-requests"),
	"repositories": require("./repositories"),
	"team": require("./team"),
	"team-members": require("./team-members"),
	"user": require("./user"),
};
