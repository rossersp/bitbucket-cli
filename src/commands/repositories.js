var program = require('commander'),
    moment = require('moment'),
    api = require('../api'),
    logger = require('../logger'),
    promptSave = require('../promptSave');

function outputRepo(repo, showDetail, opts) {
    logger.title(repo.full_name);

    logger.info('Scm', repo.scm);
    logger.info('Private', repo.is_private);

    if (showDetail) {
        var created = moment(repo.created_on);
        logger.info('Created', created.format('DD/MM/YYYY HH:mm'));

        var updated = moment(repo.updated_on);
        logger.info('Updated', updated.format('DD/MM/YYYY HH:mm'));
    }

    if (repo.language) {
        logger.info('Language', repo.language);
    }

    if (showDetail && opts.browse) {
        logger.href(repo.links.html.href);
    }
}

program
    .command('repositories [owner] [slug]')
    .alias('repos')
    .description('get a list of or single repository')
    .option('-p, --page <page>', 'Set page number', 1)
    .option('-l, --limit <limit>', 'Set page limit', 10)
    .option('-b, --browse', 'Show href to browse resource')
    .option('-f, --noprompt', 'Skip prompts')
    .option('--save', 'Save options')
    .action(function(owner, slug, options) {
        options.owner = owner;
        options.slug = slug;
        require('../preferences')()
            .then(function(preferences) {
                return promptSave({
                    properties: {
                        owner: {
                            pattern: /^\w+$/,
                            description: 'Enter the owner',
                            message: 'Owner must be supplied.',
                            required: true,
                            promptSave: !options.owner && !options.noprompt,
                            save: !!options.save
                        },
                        slug: {
                            pattern: /^[\w\-]+$/,
                            description: 'Enter the project slug',
                            required: false,
                            promptSave: !options.slug && !options.noprompt,
                            save: !!options.save
                        },
                    }
                }, options);

            })
            .then(function(options) {
                return api.v2.repositories.get(options.owner, options.slug, options);
            })
            .then(function(body) {
                if (Array.isArray(body.values)) {
                    body.values.forEach(function(repo) {
                        outputRepo(repo, false, options);
                    });
                    return;
                }
                outputRepo(body, true, options);
            })
            .catch(logger.error);
    });
