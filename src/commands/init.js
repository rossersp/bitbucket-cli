var program = require('commander'),
    logger = require('../logger'),
    prompt = require('prompt'),
    promptSave = require('../promptSave'),
    Promise = require('bluebird'),
    chalk = require('chalk'),
    moment = require('moment'),
    _ = require('lodash'),
    util = require('util');

program
    .command('init')
    .description('initialize the bitbucket configuration.')
    .option('--slug [slug]', 'the project slug')
    .option('--owner [owner]', 'the owning account')
    .option('--key [key]', 'the bitbucket key')
    .option('--private [private]', 'the bitbucket private key')
    .option('--preferenceType [global|local]', 'what kind of resource this is')
    .option('--save', 'Save options')
    .action(function(options) {
        require('../preferences')()
            .then(function(preferences) {
                //init forces save
                options.save = true;
                promptSave({
                    properties: {
                        owner: {
                            pattern: /^\w+$/,
                            description: 'Enter the owner',
                            message: 'Owner must be supplied.',
                            required: false,
                            promptSave: false,
                            save: true
                        },
                        slug: {
                            pattern: /^[\w\-]+$/,
                            description: 'Enter the project slug',
                            message: 'Slug must be supplied.',
                            required: false,
                            promptSave: false,
                            save: true
                        },
                        key: {
                            description: 'Enter the bitbucket key',
                            required: false,
                            promptSave: false,
                            save: true
                        },
                        private: {
                            description: 'Enter the bitbucket private key',
                            required: false,
                            promptSave: false,
                            save: true
                        }
                    }
                }, options);
            });
    });
