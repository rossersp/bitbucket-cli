var program = require('commander'),
api = require('../api'),
logger = require('../logger'),
promptSave = require('../promptSave');

function getTeamMembers(teamName, opts) {
    api.v2.teams.members(teamName, opts)
        .then(function(body) {
            body.values.forEach(function(member) {
                logger.info(member.username, member.display_name);
            });
        })
        .catch(logger.error);
}

program
    .command('team-members <teamName>')
    .description('')
    .option('-p, --page <page>', 'Set page number', 1)
    .option('-l, --limit <limit>', 'Set page limit', 10)
    .option('-f, --noprompt', 'Skip prompts')
    .option('--save', 'Save options')
    .action(function(teamName, options) {
        options.teamName = teamName;
        require('../preferences')()
            .then(function(preferences) {
                return promptSave({
                    properties: {
                        teamName: {
                            pattern: /^[\w\-]+$/,
                            description: 'Enter the team',
                            message: 'Team must be supplied.',
                            required: true,
                            promptSave: !options.owner && !options.noprompt,
                            save: !!options.save
                        },
                    }
                }, options);
            })
            .then(function(options) {
                return api.v2.teams.members(options.teamName, options);
            })
            .then(function(body) {
                if((body.values||[]).length == 0){
                    logger.success('No team members found.');
                }
                body.values.forEach(function(member) {
                    logger.info(member.username, member.display_name);
                });
            })
            .catch(logger.error);
    });
