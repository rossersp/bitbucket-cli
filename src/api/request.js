var Promise = require('bluebird'),
    chalk = require('chalk'),
    logger = require('../logger'),
    util = require('util');

module.exports = function(opts) {
    return require('../preferences')()
        .then(function(preferences) {
            return Promise.promisify(require('request')
                    .defaults({
                        baseUrl: 'https://bitbucket.org/api/',
                        oauth: {
                            consumer_key: preferences.bitbucketKey,
                            consumer_secret: preferences.bitbucketSecret
                        },
                        json: true
                    }))(opts)
                .spread(function(res, body) {
                    if (!/^2\d\d$/.test(res.statusCode)) {
                        if (!!res.body) {
                            logger.error({
                                message: res.statusCode + ': ' + util.inspect(body)
                            });
                        } else {
                            logger.error({
                                message: 'no body provided'
                            });
                        }
                        if (body && body.error) {
                            throw Error(body.error.message)
                        }
                        throw Error('could not complete request');

                    }

                    return Promise.resolve(body);
                });
        });
};
