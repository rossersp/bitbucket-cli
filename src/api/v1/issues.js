var request = require('../request'),
    urlencode = require('urlencode'),
    _ = require('lodash'),
    Promise = require('bluebird'),
    logger = require('../../logger');

module.exports = {

    list: function(owner, slug, options) {
        options = options || {};
        options.limit = options.limit || 15;
        var url = '/1.0/repositories/' + owner + '/' + slug + '/issues';
        var rq = request({
            uri: url,
            qs: {
                limit: !options.all ? options.limit : 1,
                start: options.start,
                search: options.search,
                sort: options.sort,
                title: options.title,
                content: options.issueDescription,
                version: options.version,
                milestone: options.milestone,
                component: options.component,
                kind: options.kind,
                status: options.status,
                responsible: options.responsible,
                reportedBy: options.reportedBy
            }
        });

        if (!options.all) {
            return rq;
        } else {
            return rq.then(function(body) {
                var pages = [];
                //note: bitbucket allows a maximum of 50 records
                options.limit = 50;
                for (var page = 0; page < Math.ceil(body.count / options.limit); page++) {
                    pages.push(request({
                        uri: url,
                        qs: {
                            limit: options.limit,
                            start: page * options.limit,
                            search: options.search,
                            sort: options.sort,
                            title: options.title,
                            content: options.issueDescription,
                            version: options.version,
                            milestone: options.milestone,
                            component: options.component,
                            kind: options.kind,
                            status: options.status,
                            responsible: options.responsible,
                            reportedBy: options.reportedBy
                        }
                    }));
                }
                return Promise.all(pages).then(function(pageResults) {
                    var allIssues = _.reduce(pageResults, function(issues, page) {
                        return  issues.concat(page.issues);
                    }, []);

                    return {
                        issues: allIssues,
                        start: 0,
                        count: allIssues.length
                    }
                });

            });
        }
    },
    get: function(owner, slug, issue) {
        return request({
            uri: '/1.0/repositories/' + owner + '/' + slug + '/issues/' + issue
        });
    },
    create: function(owner, slug, options) {
        var form = _.reduce({
            title: options.title,
            content: urlencode(options.issueDescription || ''),
            kind: options.kind,
            status: options.status,
            priority: options.priority,
            milestone: options.milestone,
            component: options.component,
            responsible: options.assignee
        }, function(obj, value, key) {
            if (!!value) {
                obj[key] = value;
            }
            return obj;
        }, {});

        return request({
            method: 'POST',
            uri: '/1.0/repositories/' + owner + '/' + slug + '/issues',
            form: form
        });
    },
    update: function(owner, slug, issue, options) {
        var form = _.reduce({
            title: options.title,
            content: !!options.issueDescription ? urlencode(options.issueDescription) : null,
            kind: options.kind,
            status: options.status,
            priority: options.priority,
            milestone: options.milestone,
            component: options.component,
            responsible: options.assignee
        }, function(obj, value, key) {
            if (!!value) {
                obj[key] = value;
            }
            return obj;
        }, {});

        return request({
            method: 'PUT',
            uri: '/1.0/repositories/' + owner + '/' + slug + '/issues/' + issue,
            form: form
        });
    },
    remove: function(owner, slug, issue) {
        return request({
            method: 'DELETE',
            uri: '/1.0/repositories/' + owner + '/' + slug + '/issues/' + issue,
        });
    },
    comments: {
        list: function(owner, slug, issue) {
            var url = '/1.0/repositories/' + owner + '/' + slug + '/issues/' + issue + '/comments';
            return request({
                uri: url,
            });
        },
        get: function(owner, slug, issue, comment) {
            var url = '/1.0/repositories/' + owner + '/' + slug + '/issues/' + issue + '/comments/' + comment;
            return request({
                uri: url
            });
        },
        create: function(owner, slug, issue, options) {
            return request({
                method: 'POST',
                url: '/1.0/repositories/' + owner + '/' + slug + '/issues/' + issue + '/comments',
                form: {
                    content: options.content
                }
            });
        },
        update: function(owner, slug, issue, comment, options) {
            return request({
                method: 'PUT',
                url: '/1.0/repositories/' + owner + '/' + slug + '/issues/' + issue + '/comments/' + comment,
                form: {
                    content: options.content
                }
            });
        }
    }
};
