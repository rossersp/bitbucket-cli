module.exports = exports = {
	"pullRequests": require("./pull-requests"),
	"repositories": require("./repositories"),
	"teams": require("./teams"),
	"users": require("./users"),
};
