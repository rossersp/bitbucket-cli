# Bitbucket CLI+
Interface with Bitbucket via their [RESTful api](https://api.bitbucket.org) with the command line tool.

This version is a fork of [Liam Moat's](https://bitbucket.org/liammoat/bitbucket-cli) excellent original Bitbucket CLI, with a few differences:

* **Includes some of the v1 functionality.**  This version provides the ability to add, edit, and remove issues as well as add and edit comments on those issues.
* **Provides a per-project configuration.** While not necessary, each project that uses the Bitbucket CLI can have its own key and secret, as well as its own owner and slug stored in a local configuration.  This helps cut down on command verbosity, especially with the v1 functions.
* **Prompts.** If you miss a required parameter, the CLI will prompt you for a value.  (Note that is is overridable using --noprompt on any command.)


> **Note:** This tool is not affliated with Bitbucket or Atlassian. It is a third party library that simply interfaces with Bitbucket's API.

## Getting Started
The Bitbucket CLI is installed and managed via [npm](https://npmjs.org/), the [Node.js](http://nodejs.org/) package manager.

Before setting up ensure that your [npm](https://npmjs.org/) is up-to-date by running ```npm update -g npm``` (this might require sudo on certain systems).

### Installing 
In order to get started, you'll want to install the command line interface (CLI) globally. You may need to use sudo (for OSX, \*nix, BSD etc) or run your command shell as Administrator (for Windows) to do this.

```
npm install -g bitbucket-cli-plus
```
This will put the ```bitbucket``` command in your system path, allowing it to be run from any directory.

To confirm it has installed correctly run ```bitbucket --version```.

### Obtaining an OAuth Key
The CLI uses OAuth when communicating with Bitbucket. Before using the CLI you need to obtain a consumer key and secret.

Bitbucket explain how to do this [here](https://confluence.atlassian.com/display/BITBUCKET/OAuth+on+Bitbucket#OAuthonBitbucket-Step1.CreateanOAuthkeyandsecret).

### Using the CLI
Once the CLI is installed you're good go. Get all the available commands with  ```bitbucket --help```.

There are three ways of giving the CLI you consumer settings:

1. Export the environment variables.
```
export BITBUCKET_KEY="MY_CONSUMER_KEY"
export BITBUCKET_SECRET="MY_CONSUMER_SECRET"
bitbucket ...
```

2. Specify the environment variables directly.
```
BITBUCKET_KEY="MY_CONSUMER_KEY" BITBUCKET_SECRET="MY_CONSUMER_SECRET" bitbucket ...
```

3. Use `bitbucket init`.  This will prompt you for not only the key and secret, but also (optionally) the project slug and owner.

## Preferences
This version of the Bitbucket CLI has an optional preference file that can be included in any directory at or above the project (local), or with the install of Bitbucket CLI itself (global).  The options passed into the CLI are preferred to the environment variables, the environment variables to the local preferences, local preferences to global preferences, and finally, the global preferences are preferred over some pre-selected (hopefully sane) defaults, limited for now to v1 functionality.  The following describes how and when the preferences are set and saved:

* **bitbucketKey** - the Bitbucket OAuth key.  Set during `init`.
* **bitbucketSecret** - the secret part of the Bitbucket OAuth key.  Set during `init`.
* **owner** - the owner or team name that owns the project.  Set during `init` or as an argument for `-o, --owner` coupled with `--save`.
* **slug** - the project slug for the project.  Set during `init` or as an argument for `-s, --slug` coupled with `--save`.
* **kind** - the default issue type.  Can be set from `--save` for an issue subcommand, or as an argument for `--kind` in an issue subcommand.  Defaults to `enhancement`.
* **priority** - the default issue priority.  Can be set from `--save` for an issue subcommand, or as an argument for `--priority` in an issue subcommand.  Defaults to `major`.
* **status** - the default issue status.  Can be set from `--save` for an issue subcommand, or as an argument for `--status` in an issue subcommand.  Defaults to `new`.

