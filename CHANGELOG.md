##0.1.4
* Fixes #31 - Environment variables are now correctly reduced for preferences.

##0.1.3
* Adds #12 - Can now add `--all` to `bitbucket issues` to get all issues.  This works by issuing a request for one issue as the BitBucket API returns back the number of total issues.  Then, a series of requests are issued to get all issues using the maximum limit of 50.

##0.1.2
* Fixes #30 - No upstream .git directory meant that the .git path was undefined, causing jsonfile to crash.

##0.1.1
* Fixes #29 - Local `.bbcli.rc` files were being required regardless of if the upstream repository folder had one.

##0.1.0
Initial release.
